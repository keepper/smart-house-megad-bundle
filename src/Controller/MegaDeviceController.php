<?php
namespace Keepper\SmartHouseMegadBundle\Controller;

use Keepper\SmartHouse\Core\Button\ButtonInterface;
use Keepper\SmartHouse\Core\Button\PressModeInterface;
use Keepper\SmartHouse\Core\Registry\RegistryInterface;
use Keepper\SmartHouse\Core\Sensor\SensorInterface;
use Keepper\SmartHouseMegadBundle\Service\PortMapper;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\NullLogger;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\KernelInterface;
//use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * @Route(service="SmartHouse.MegaD.CallbackController")
 */
class MegaDeviceController {

    use LoggerAwareTrait;

    /**
     * @var PortMapper
     */
    private $mapper;

    /**
     * @var RegistryInterface
     */
    private $registry;

    /**
     * @var KernelInterface
     */
    private $kernel;

    private $stdErr;

    private $stdOut;

    public function __construct(
        PortMapper $portMapper,
        RegistryInterface $registry,
        KernelInterface $kernel,
        string $stdErr = '/dev/null',
        string $stdOut = '/dev/null'
    ) {
        $this->mapper = $portMapper;
        $this->registry = $registry;
        $this->kernel = $kernel;
        $this->stdErr = $stdErr;
        $this->stdOut = $stdOut;
        $this->setLogger(new NullLogger());
    }

    /**
     * @Route("/megad", name="mega-device-callback")
     */
    public function callbackAction(Request $request) {
        if ($request->get('st') == 1) {
            $path = realpath($this->kernel->getRootDir().'/../');
            $cmd = $path.'/bin/console smart:sync ';
            $this->startBackgroundProcess($cmd, null, $this->stdOut, $this->stdErr);
            return new Response('');
        }

        $port = $request->get('pt');
        if ( is_null($port) ) {
            $this->logger->warning('Вызов callback url без передачи номера порта');
            return new Response('error', 500);
        }

        $uuid = $this->mapper->getUuidByPort($port);
        if ( is_null($uuid) ) {
            $this->logger->debug('callback запроса от не сконфигурированного порта. Port='.$port);
            return new Response('d');
        }

        $device = $this->registry->getByUuid($uuid);

        if ( $device instanceof ButtonInterface ) {
            $pressMode = PressModeInterface::SINGLE;
            $m = $request->get('m');
            $click = $request->get('click');
            if( $click == 2 ) {
                $pressMode = PressModeInterface::DOUBLE;
            } else if ($m == 2) {
                $pressMode = PressModeInterface::LONG;
            }

            $path = realpath($this->kernel->getRootDir().'/../');
            $cmd = $path.'/bin/console smart:ui-button '.$device->uuid().' press '.$pressMode;
            $this->startBackgroundProcess($cmd, null, $this->stdOut, $this->stdErr);
            return new Response('');

        } else if ($device instanceof SensorInterface) {

            $path = realpath($this->kernel->getRootDir().'/../');
            $cmd = $path.'/bin/console smart:ui-sensor '.$device->uuid();
            $this->startBackgroundProcess($cmd, null, $this->stdOut, $this->stdErr);

        } else {
            $this->logger->warning('Не обрабатываемый callback вызов от uuid='.$device->uuid());
            return new Response('error', 500);
        }

        return new Response('');
    }

    private function startBackgroundProcess(
        $command,
        $stdin = null,
        $redirectStdout = null,
        $redirectStderr = null,
        $cwd = null,
        $env = null,
        $other_options = null
    ) {
        $descriptorspec = array(
            1 => is_string($redirectStdout) ? array('file', $redirectStdout, 'a') : array('pipe', 'a'),
            2 => is_string($redirectStderr) ? array('file', $redirectStderr, 'a') : array('pipe', 'a'),
        );
        if (is_string($stdin)) {
            $descriptorspec[0] = array('pipe', 'r');
        }
        $proc = proc_open($command, $descriptorspec, $pipes, $cwd, $env, $other_options);
        if (!is_resource($proc)) {
            throw new \Exception("Failed to start background process by command: $command");
        }
        if (is_string($stdin)) {
            fwrite($pipes[0], $stdin);
            fclose($pipes[0]);
        }
        if (!is_string($redirectStdout)) {
            fclose($pipes[1]);
        }
        if (!is_string($redirectStderr)) {
            fclose($pipes[2]);
        }
        return $proc;
    }
}