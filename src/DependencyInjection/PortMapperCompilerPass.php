<?php
namespace Keepper\SmartHouseMegadBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

class PortMapperCompilerPass implements CompilerPassInterface {

    const TAG = 'megad-port';

    /**
     * You can modify the container here before it is dumped to PHP code.
     */
    public function process(ContainerBuilder $container) {
        $mapper = $container->getDefinition('SmartHouse.MegaD.PortMapper');

        $uuidServices = $container->findTaggedServiceIds(self::TAG);

        foreach ($uuidServices as $id => $tags) {
            $mapper->addMethodCall(
                'addMapping',
                [
                    $tags[0]['port'],
                    new Reference($id)
                ]
            );
        }
    }
}