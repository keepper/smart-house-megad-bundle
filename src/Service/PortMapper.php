<?php
namespace Keepper\SmartHouseMegadBundle\Service;

use Keepper\SmartHouse\Core\UuidInterface;

class PortMapper {

    private $map = [];

    public function addMapping(int $portNumber, UuidInterface $device) {
        $this->map[$portNumber] = $device->uuid();
    }

    public function getUuidByPort(int $portNumber): ?string {
        if ( !array_key_exists($portNumber, $this->map) ) {
            return null;
        }

        return $this->map[$portNumber];
    }
}