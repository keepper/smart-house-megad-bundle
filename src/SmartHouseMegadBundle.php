<?php
namespace Keepper\SmartHouseMegadBundle;

use Keepper\ConventionSkeleton\SkeletonBundle;
use Keepper\SmartHouseMegadBundle\DependencyInjection\PortMapperCompilerPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class SmartHouseMegadBundle extends SkeletonBundle {

    public function build(ContainerBuilder $container) {
        parent::build($container);
        $container->addCompilerPass(new PortMapperCompilerPass());
    }
}